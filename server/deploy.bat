@echo off
title Hello World!
@chcp 852

echo.
echo What should the version be?
echo.
echo Version no:
set /p version=">> "
echo O! Witaj %version% !
ping 1.1.1.1 -n 1 -w 705 > nul

docker build -t mrgregf/graphqlapp:%version% .\
docker push mrgregf/graphqlapp:%version%
ssh root@206.81.16.60 "docker pull mrgregf/graphqlapp:%version% && docker tag mrgregf/graphqlapp:%version% dokku/api:latest && dokku deploy api latest"

exit
