import { MyContext } from "src/types";
import { MiddlewareFn } from "type-graphql/dist/interfaces/Middleware";


export const ResolveTime: MiddlewareFn<MyContext> = async ({ info }, next) => {
   const start = Date.now();
   await next();
   const resolveTime = Date.now() - start;
   console.log(`ResolveTime middleware: ${info.parentType.name}.${info.fieldName} [${resolveTime} ms]`);
};