import { MyContext } from 'src/types';
import { Arg, Ctx, Field, FieldResolver, InputType, Int, Mutation, ObjectType, Query, Resolver, Root, UseMiddleware } from 'type-graphql';
import { getConnection } from 'typeorm';
import { Post } from '../entities/Post';
import { Updoot } from '../entities/Updoot';
import { User } from '../entities/User';
import { isAuth } from '../middleware/isAuth';
import { ResolveTime } from '../middleware/ResolveTime';

@InputType()
class PostInput {
    @Field()
    title: string
    @Field()
    text: string
}

@ObjectType()
class PaginatedPosts {
    @Field(() => [Post])
    posts: Post[]
    @Field()
    hasMore: boolean
}

@Resolver(Post)
export class PostResolver {
    @FieldResolver(() => String)
    async textSnippet(@Root() post: Post) {
        return post.text.slice(0, 50);
    }

    @FieldResolver(() => User)
    async creator(
        @Root() post: Post,
        @Ctx() { userLoader }: MyContext
    ) {
        return userLoader.load(post.creatorId);
    }

    @FieldResolver(() => User)
    async voteStatus(
        @Root() post: Post,
        @Ctx() { updootLoader, req }: MyContext
    ) {

        if (!req.session.userId) {
            return null
        }

        const updoot = await updootLoader.load({
            postId: post.id, userId: req.session.userId
        })

        return updoot ? updoot.value : null
    }


    @Mutation(() => Boolean)
    @UseMiddleware(isAuth)
    async vote(
        @Arg('postId', () => Int) postId: number,
        @Arg('value', () => Int) value: number,
        @Ctx() { req }: MyContext
    ) {
        const isUpdoot = value !== -1;
        const realValue = isUpdoot ? 1 : -1
        const { userId } = req.session;
        const updoot = await Updoot.findOne({ where: { postId, userId } })

        if (updoot && realValue !== updoot.value) {
            await getConnection().transaction(async (em) => {
                await em.query(`
                    UPDATE updoot
                    SET "value" = ${realValue}
                    WHERE "userId" = ${userId} AND "postId" = ${postId};
                `);

                await em.query(`
                    UPDATE post
                    SET points = points + ${2 * realValue}
                    WHERE id = ${postId};
                `);
            });
        } else if (!updoot) {
            await getConnection().transaction(async (em) => {
                await em.query(`
                    INSERT INTO "updoot"("value", "userId", "postId") 
                    VALUES (${value}, ${userId}, ${postId});
                `);
                await em.query(`
                    UPDATE post
                    SET points = points + ${realValue}
                    WHERE id = ${postId};
                `);
            });
        }
        return true;
    }

    @Query(() => PaginatedPosts)
    async posts(
        @Arg('limit', () => Int) limit: number,
        @Arg('cursor', () => String, { nullable: true }) cursor: string | null
    ): Promise<PaginatedPosts> {

        const realLimit = Math.min(50, limit);
        const realLimitPlusOne = realLimit + 1;
        let query_params: any[] = [realLimitPlusOne];

        if (cursor) {
            query_params.push(new Date(parseInt(cursor)));
        };

        const posts = await getConnection().query(
            ` 
            SELECT p.*
            FROM post p
            ${cursor ? `WHERE p."createdAt" < $2` : ""}
            ORDER BY p."createdAt" DESC
            LIMIT $1
        `,
            query_params
        );


        // const qb = getConnection()
        //     .getRepository(Post)
        //     .createQueryBuilder("p")
        //     .innerJoinAndSelect("p.creator", "u", 'u.id = p."creatorId"')
        //     .orderBy('p."createdAt"', 'DESC')
        //     .take(realLimitPlusOne)

        return ({
            posts: posts.slice(0, realLimit),
            hasMore: posts.length === realLimitPlusOne
        });
    }

    @Query(() => Post, { nullable: true })
    async post(
        @Arg('id', () => Int) id: number,
    ): Promise<Post | undefined> {

        // const query = `
        // SELECT 
        //     p.*,
        // FROM public.post p
        // WHERE p.id IN (${id})
        // `
        // const post2 = await getConnection().query(query)
        // // return (post[0]);

        const post = await Post.findOne(id, { relations: ["creator"] });
        return post;
    }

    @Mutation(() => Post)
    @UseMiddleware(ResolveTime, isAuth)
    async createPost(
        @Arg('input') input: PostInput,
        @Ctx() { req }: MyContext
    ): Promise<Post> {
        return Post.create({
            ...input,
            creatorId: req.session.userId,
        }).save();
    }

    @Mutation(() => Post)
    async updatePost(
        @Arg('id') id: number,
        @Arg('title', () => String, { nullable: true }) title: string,
    ): Promise<Post | null> {
        const post = await Post.findOne(id);
        if (!post) {
            return null;
        }
        if (typeof title !== 'undefined') {
            await Post.update({ id }, { title });
        }
        return post;
    }

    @Mutation(() => Boolean)
    @UseMiddleware(isAuth)
    async deletePost(
        @Arg('id', () => Int) id: number,
        @Ctx() { req }: MyContext
    ): Promise<boolean> {
        await Post.delete({ id, creatorId: req.session.userId })
        return true;
    }
}
