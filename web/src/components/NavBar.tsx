import { Box, Button, Flex, Heading, Link } from '@chakra-ui/core'
import React from 'react';
import NextLink from 'next/link'
import { useLogoutMutation, useMeQuery } from '../generated/graphql';
import { isServer } from '../utils/isServer';
import { useRouter } from 'next/router'
import { useApolloClient } from '@apollo/client';

interface NavBarProps {

}

export const NavBar: React.FC<NavBarProps> = ({ }) => {
    const router = useRouter();
    const [logout, { loading: LogoutFetching }] = useLogoutMutation();
    const { data, loading } = useMeQuery({
        skip: isServer(),
    });
    const apolloClient = useApolloClient();
    let body = null;

    if (loading) {

    } else if (!data?.me) {
        body = (
            <>
                <NextLink href='/login'>
                    <Link mr={2}> login </Link>
                </NextLink>
                <NextLink href='/register'>
                    <Link mr={2}> register</Link>
                </NextLink>
            </>
        )
    } else {
        body = (
            <Flex>
                <Box mr={4}>
                    {data.me.username}
                </Box>
                <Button
                    onClick={async () => {
                        await logout();
                        await apolloClient.resetStore();
                    }}
                    isLoading={LogoutFetching}
                    variant='link'>
                    logout
                </Button>
            </Flex >
        );
    }

    return (
        <Flex bg='tan' p={4} align='center'>
            <NextLink href='/'>
                <Link>
                    <Heading>LiReddit</Heading>
                </Link>
            </NextLink>

            <Box ml='auto'>
                {body}
            </Box>
        </Flex >
    );
}