import { ApolloCache } from '@apollo/client';
import { Flex, IconButton } from '@chakra-ui/core';
import React, { useState } from 'react';
import { PostSnippetFragment, useVoteMutation, VoteMutation } from '../generated/graphql';
import gql from "graphql-tag";

interface UpdootSectionProps {
   post: PostSnippetFragment;
}

const updateAfterVote = (
   value: number,
   postId: number,
   cache: ApolloCache<VoteMutation>
) => {
   const data = cache.readFragment<{
      id: number;
      points: number;
      voteStatus: number | null;
   }>({
      id: "Post:" + postId,
      fragment: gql`
       fragment _ on Post {
         id
         points
         voteStatus
       }
     `,
   });

   if (data) {
      if (data.voteStatus === value) {
         return;
      }
      const newPoints =
         (data.points as number) + (!data.voteStatus ? 1 : 2) * value;
      cache.writeFragment({
         id: "Post:" + postId,
         fragment: gql`
         fragment __ on Post {
           points
           voteStatus
         }
       `,
         data: { points: newPoints, voteStatus: value },
      });
   }
};

export const UpdootSection: React.FC<UpdootSectionProps> = ({ post }) => {
   const [vote] = useVoteMutation()
   const [loadingState, setLoadingState] = useState<'upvoot-is-loading' | 'downvoot-is-loading' | 'not-loading'>('not-loading')

   return (
      <Flex direction='column' mr={4} justifyContent='center' alignItems='center'>
         <IconButton
            onClick={async () => {
               if (post.voteStatus === 1) {
                  return;
               }
               setLoadingState('upvoot-is-loading');
               await vote({
                  variables:
                  {
                     postId: post.id,
                     value: 1
                  },
                  update: (cache) => updateAfterVote(1, post.id, cache),
               });
               setLoadingState('not-loading');
            }}
            aria-label="Call Segun"
            icon='chevron-up' size='md'
            key={post.id}
            isLoading={loadingState === 'upvoot-is-loading'}
            variantColor={post.voteStatus === 1 ? "green" : undefined}
         />
         {post.points}
         <IconButton
            onClick={async () => {
               if (post.voteStatus === -1) {
                  return;
               }
               setLoadingState('downvoot-is-loading');
               await vote({
                  variables:
                  {
                     postId: post.id,
                     value: -1
                  },
                  update: (cache) => updateAfterVote(-1, post.id, cache),
               });
               setLoadingState('not-loading');
            }}
            aria-label="Call Segun"
            icon="chevron-down"
            size='md'
            isLoading={loadingState === 'downvoot-is-loading'}
            variantColor={post.voteStatus === -1 ? "red" : undefined}
         />
      </Flex>
   );
}