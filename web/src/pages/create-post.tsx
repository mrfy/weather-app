import { Box, Button } from '@chakra-ui/core';
import { Form, Formik } from 'formik';
import { withUrqlClient } from 'next-urql';
import { useRouter } from "next/router";
import React from 'react';
import { InputField } from '../components/InputField';
import { Layout } from '../components/Layout';
import { useCreatePostMutation } from '../generated/graphql';
import { createUrqlClient } from '../utils/createUrqlClient';
import { useIsAuth } from '../utils/useIsAuth';
import { withApollo } from '../utils/withApollo';

interface CreatePostProps {
}

const CreatePost: React.FC<CreatePostProps> = ({ }) => {
   const router = useRouter();
   useIsAuth();

   const [post] = useCreatePostMutation();
   return (
      <Layout variant='small'>
         <Formik initialValues={{ title: "", text: "" }}
            onSubmit={async (values) => {
               const { errors } = await post({
                  variables: { input: values }
               });
               if (!errors) {
                  router.push('/');
               }
            }}
         >
            {({ isSubmitting }) => (
               <Form>
                  <InputField
                     name='title'
                     placeholder='title..'
                     label='Title'
                  />
                  <Box mt={4}>
                     <InputField
                        name='text'
                        placeholder='text...'
                        label='Body'
                        textarea
                     />
                  </Box>
                  <Button
                     mt='4'
                     type='submit'
                     variantColor='teal'
                     leftIcon='add'
                     isLoading={isSubmitting}
                  >
                     Create post.
                     </Button>
               </Form>
            )
            }
         </Formik>
      </Layout >
   );
}

export default withApollo({ ssr: false })(CreatePost);
