import { withUrqlClient } from 'next-urql';
import React from 'react'
import { createUrqlClient } from '../../utils/createUrqlClient';
import { useRouter } from 'next/router';
import { usePostQuery } from '../../generated/graphql';
import { Layout } from '../../components/Layout';
import { Box, Heading } from '@chakra-ui/core';
import { withApollo } from '../../utils/withApollo';

interface PostProps {
}

export const Post: React.FC<PostProps> = ({ }) => {
   const router = useRouter();
   const intId = typeof router.query.id === 'string' ? parseInt(router.query.id) : -1;
   const { data, error, loading } = usePostQuery({
      skip: intId === -1,
      variables: {
         id: intId,
      }
   })

   if (error) {
      console.log('error', error);

      return (
         <div>
            errrror
            {error.message}
         </div>
      )
   }

   if (loading) {
      return (
         <Layout>
            <div>Loading ... .. .</div>
         </Layout>
      )
   }

   if (!data?.post) {
      return (
         <Layout>
            <Box>Could not find any post</Box>
         </Layout>
      );
   }
   return (
      <Layout>
         <Heading mb={4}>{data.post.title}</Heading>
         {data?.post?.text}
      </Layout>
   );
}

export default withApollo({ ssr: true })(Post);
