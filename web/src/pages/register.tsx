import { Box, Button } from '@chakra-ui/core';
import { query } from '@urql/exchange-graphcache';
import { Form, Formik } from "formik";
import { withUrqlClient } from 'next-urql';
import { useRouter } from "next/router";
import React from 'react';
import { InputField } from '../components/InputField';
import { Wrapper } from '../components/Wrapper';
import { MeDocument, MeQuery, useRegisterMutation } from '../generated/graphql';
import { createUrqlClient } from '../utils/createUrqlClient';
import { toErrorMap } from '../utils/toErrorMap';
import { withApollo } from '../utils/withApollo';


interface registerProps { }


const Register: React.FC<registerProps> = ({ }) => {
   const router = useRouter();
   const [register] = useRegisterMutation();
   return (
      <Wrapper variant='small'>
         <Formik
            initialValues={{ email: "", username: "", password: "" }}
            onSubmit={async (values, { setErrors }) => {
               console.log(values);
               const response = await register({
                  variables: {
                     options: values
                  },
                  update: (cache, { data }) => {
                     cache.writeQuery<MeQuery>({
                        query: MeDocument,
                        data: {
                           __typename: 'Query',
                           me: data?.register.user,
                        }
                     })
                  }
               });
               console.log('rezponse ', response);

               if (response.data?.register.errors) {
                  setErrors(toErrorMap(response.data.register.errors));
               } else if (response.data?.register.user) {
                  //worked
                  router.push("/");
               }
            }}
         >
            {({ isSubmitting }) => (
               <Form>
                  <InputField
                     name='username'
                     placeholder='username'
                     label='Username'
                  />
                  <Box marginTop={4}>
                     <InputField
                        name='password'
                        placeholder='password'
                        label='Password'
                        type='password'
                     />
                  </Box>
                  <Box marginTop={4}>
                     <InputField
                        name='email'
                        placeholder='email'
                        label='Email'
                        type='email'
                     />
                  </Box>
                  <Button
                     mt='4'
                     type='submit'
                     variantColor='teal'
                     leftIcon='unlock'
                     isLoading={isSubmitting}
                  >Register
                     </Button>
               </Form>
            )
            }
         </Formik>
      </Wrapper >
   );
}

export default withApollo({ ssr: false })(Register);