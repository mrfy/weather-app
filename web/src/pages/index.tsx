import { Box, Button, Flex, Heading, IconButton, Link, Stack, Text } from '@chakra-ui/core';
import { withApollo } from "../utils/withApollo"
import NextLink from 'next/link';
import React from 'react';
import { Layout } from '../components/Layout';
import { UpdootSection } from '../components/UpdootSection';
import { useDeletePostMutation, usePostsQuery } from '../generated/graphql';

const Index = () => {
    const { data, loading, error, fetchMore, variables } = usePostsQuery({
        variables: {
            limit: 15,
            cursor: null
        },
        notifyOnNetworkStatusChange: true,
    });

    const [deletePost] = useDeletePostMutation();

    if (!data && !loading) {
        return <div>you have no data! (query failed)  {error?.message}</div>
    }
    return (
        <Layout>
            <Flex>
                <Heading> GraphQL AppAs</Heading>
                <NextLink href='/create-post' >
                    <Button variant='outline' ml='auto'>create post</Button>
                </NextLink>
            </Flex>
            <br />
            {!data && loading ? (
                <div>Loading...</div>
            ) : (
                    <Stack spacing={8}>
                        IconButton
                        {data!.posts.posts.map(p =>
                            !p ? null
                                : (

                                    <Flex p={5} shadow="md" borderWidth="1px" key={p.id} >
                                        <UpdootSection post={p}></UpdootSection>
                                        <Box>
                                            <NextLink href="/post/[id]" as={`/post/${p.id}`}>
                                                <Link>
                                                    <Heading fontSize="xl">{p.title}</Heading>
                                                </Link>
                                            </NextLink>
                                            <Text>posted by {p.creator.username}</Text>
                                            <Text mt={4}>{p.textSnippet}</Text>
                                        </Box>
                                        <Box ml='auto' mr={0}>
                                            <IconButton
                                                aria-label='Delete Post'
                                                icon='delete'
                                                onClick={() => {
                                                    deletePost({
                                                        variables: { id: p.id },
                                                        update: (cache) => {
                                                            cache.evict({ id: 'Post:' + p.id });
                                                        }
                                                    })
                                                }}>
                                                delete post
                                    </IconButton>
                                        </Box>
                                    </Flex>
                                ))}
                    </Stack>
                )
            }
            {
                data && data.posts.hasMore ?
                    <Flex>
                        <Button
                            onClick={() => {
                                fetchMore({
                                    variables: {
                                        limit: variables?.limit,
                                        cursor: data.posts.posts[data.posts.posts.length - 1].createdAt,
                                    }
                                });
                            }}
                            isLoading={loading} margin='auto' my={8}> Load more </Button>
                    </Flex> : null
            }

        </Layout >
    )
};

export default withApollo({ ssr: true })(Index);