import { Box, Button } from '@chakra-ui/core';
import { Form, Formik } from 'formik';
import { withUrqlClient } from 'next-urql';
import React, { useState } from 'react';
import { InputField } from '../components/InputField';
import { Wrapper } from '../components/Wrapper';
import { useForgotPasswordMutation } from '../generated/graphql';
import { createUrqlClient } from '../utils/createUrqlClient';
import { withApollo } from '../utils/withApollo';

const ForgotPassword: React.FC<{}> = ({ }) => {
   const [complete, setComplete] = useState(false);
   const [forgotPassword] = useForgotPasswordMutation()
   return (
      <Wrapper variant='small'>
         <Formik initialValues={{ email: "" }}
            onSubmit={async (values, { setErrors }) => {
               await forgotPassword({ variables: values });
               setComplete(true);
            }}
         >
            {({ isSubmitting }) =>
               complete ? (
                  <Box>If account exists, we sent you an email</Box>
               ) : (
                     <Form>
                        <Box marginTop={4}>
                           <InputField
                              name='email'
                              placeholder='email'
                              label='Email'
                              type='email'
                           />
                        </Box>
                        <Button
                           mt='4'
                           type='submit'
                           variantColor='teal'
                           leftIcon='unlock'
                           isLoading={isSubmitting}
                        >Forgot password
               </Button>
                     </Form>
                  )
            }
         </Formik>
      </Wrapper >
   );
}

export default withApollo({ ssr: false })(ForgotPassword);
